lang = [1, 2, 3]

for item in lang do
	puts item
end

puts "======================="

lang = [3, 4, 5]

lang.each do |item|
	puts item
end